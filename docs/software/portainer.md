# Portainer

[Portainer](https://www.portainer.io/) is a Docker management interface, for the more advanced user.

## Access

It is available at [https://portainer.{{ domain }}/](https://portainer.{{ domain }}/) or [http://portainer.{{ domain }}/](http://portainer.{{ domain }}/)

{% if enable_tor %}
It is also available via Tor at [http://portainer.{{ tor_domain }}/](http://portainer.{{ tor_domain }}/)
{% endif %}
